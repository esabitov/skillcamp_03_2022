﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Runtime.Serialization;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Text;
using Newtonsoft.Json;
using WpfApp6.Models;


namespace WpfApp6
{


    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private string _userToken = "";
        public MainWindow()
        {

            InitializeComponent();

        }

        private void MakeRequestButton_Click(object sender, RoutedEventArgs e)
        { 
            WebClient client = new WebClient();
            var result =   client.DownloadString(UrlTextBox.Text);
            ResponseDataTextBlock.Text = result;
        }

        private void MakePostRequestButton_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                WebRequest request = WebRequest.Create(UrlTextBox.Text);
                request.Method = "POST";
                var byteArray = Encoding.UTF8.GetBytes(RequestDataTextBox.Text);
                
                var reqStream = request.GetRequestStream();
                reqStream.Write(byteArray, 0, byteArray.Length);
                var response = request.GetResponse();
                var stream = response.GetResponseStream();
                var reader = new StreamReader(stream);
                var result = reader.ReadToEnd();
                ResponseDataTextBlock.Text = result;
            }
            catch (WebException ex)
            {
                MessageBox.Show("Произошла ошибка в заимодействии с сервером "+ ((HttpWebResponse)ex.Response).StatusCode);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Произошла какая-то другая ошибка");
            }



        }

        private void MakeMskDataRequestButton_OnClick(object sender, RoutedEventArgs e)
        {
            WebClient client = new WebClient();
            var result = client.DownloadString("https://hakta.pro/getData.php");
            // object -> JSON String   Serialize
            // JSON string -> object   Deserialize

            List<StatsItem> items = JsonConvert.DeserializeObject<List<StatsItem>>(result);
            DataListView.ItemsSource = items;
        }

        private void SignInButton_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                WebRequest request = WebRequest.Create("https://wsa2021.mad.hakta.pro/api/signin/");
                request.Method = "POST";
                request.ContentType = "application/json";

                var credentials = new SignInRequest();
                credentials.login = LoginTextBox.Text;
                credentials.password = PassTextBox.Password;

                var jsonString = JsonConvert.SerializeObject(credentials);


                var byteArray = Encoding.UTF8.GetBytes(jsonString);

                var reqStream = request.GetRequestStream();
                reqStream.Write(byteArray, 0, byteArray.Length);
                var response = request.GetResponse();
                var stream = response.GetResponseStream();
                var reader = new StreamReader(stream);
                var result = reader.ReadToEnd();
                var signInResponse = JsonConvert.DeserializeObject<SignInResponse>(result);
                _userToken = signInResponse.data.token;
                MessageBox.Show("Вы успешно вошли");
            }
            catch (WebException ex)
            {
                MessageBox.Show("Произошла ошибка в заимодействии с сервером " + ((HttpWebResponse)ex.Response).StatusCode);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Произошла какая-то другая ошибка");
            }
        }

        private void GetQRButton_OnClick(object sender, RoutedEventArgs e)
        {
             
            WebClient client = new WebClient();
            client.Headers.Add("Authorization", "Bearer "+ _userToken);
            client.Headers.Add("Token", this._userToken);
            var result = client.DownloadString("https://wsa2021.mad.hakta.pro/api/user_qr");
            var qrData = JsonConvert.DeserializeObject<GetQRResponse>(result);
            var bm = new BitmapImage(new Uri(qrData.data, UriKind.Absolute));
            QRCodeImage.Source = bm;

        }
    }
}
