﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp6.Models
{

    public class SignInResponse
    {
        public Data data { get; set; }
        public bool success { get; set; }
    }

    public class Data
    {
        public string id { get; set; }
        public string login { get; set; }
        public string name { get; set; }
        public string token { get; set; }
    }

     
}
