﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp6
{
    class StatsItem
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int ReportingYear { get; set; }
        public int global_id { get; set; }
        public string ReportingPeriod { get; set; }
        public float PlannedIndicators { get; set; }
        public float ActualIndicators { get; set; }
        public string PlanImplementation { get; set; }
        public string ProjectedImplementation { get; set; }
        public string NOTE { get; set; }
        public override string ToString()
        {
            return this.Name;
        }
    }
}
